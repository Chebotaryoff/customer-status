<?php

namespace Chebotaryov\CustomerStatus\CustomerData;

use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Customer\CustomerData\SectionSourceInterface;

/**
 * Customer status section
 */
class Status implements SectionSourceInterface
{
    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    private $currentCustomer;

    /**
     * Init dependencies.
     *
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
     */
    public function __construct(
        CurrentCustomer $currentCustomer
    ) {
        $this->currentCustomer = $currentCustomer;
    }

    /**
     * {@inheritdoc}
     */
    public function getSectionData()
    {
        if (!$this->currentCustomer->getCustomerId()) {
            return [];
        }

        $customer = $this->currentCustomer->getCustomer();
        /** @var \Magento\Framework\Api\AttributeValue $customerStatus */
        $customerStatus = $customer->getCustomAttribute('customer_status');
        $value = (isset($customerStatus)) ? $customerStatus->getValue(): null;
        return [
            'customer_status' => $value,
        ];
    }
}
