<?php

namespace Chebotaryov\CustomerStatus\Controller\Account;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey\Validator;

class CustomerStatusPost extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $session;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    private $formKeyValidator;

    /**
     * Init dependencies.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        Validator $formKeyValidator
    ) {
        $this->session = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->formKeyValidator = $formKeyValidator;
        parent::__construct($context);
    }

    /**
     * Update customer status attribute
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $validFormKey = $this->formKeyValidator->validate($this->getRequest());

        if ($validFormKey && $this->getRequest()->isPost()) {
            $postParams = $this->getRequest()->getPostValue();
            if (isset($postParams['customer-status'])) {
                try {
                    $customer = $this->customerRepository->getById($this->session->getCustomerId());
                    $customer->setCustomAttribute(
                        'customer_status',
                        trim($postParams['customer-status'])
                    );
                    $this->customerRepository->save($customer);
                    $this->messageManager->addSuccessMessage(__('You saved the status.'));
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(__('We can\'t save the status.'));
                }
            }
        }

        return $resultRedirect->setPath('customer/account/customerstatus');
    }
}
