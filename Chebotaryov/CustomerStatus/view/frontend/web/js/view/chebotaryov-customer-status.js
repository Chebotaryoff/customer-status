define([
    'uiComponent',
    'Magento_Customer/js/customer-data'
], function (Component, customerData) {
    'use strict';

    return Component.extend({
        /** @inheritdoc */
        initialize: function () {
            this._super();
            this.chebotaryov_customer_status = customerData.get('chebotaryov-customer-status');
        }
    });
});

