<?php

namespace Chebotaryov\CustomerStatus\Block\Form\Status;

/**
 * Customer status attribute edit form block
 */
class Edit extends \Magento\Customer\Block\Account\Dashboard
{
    /**
     * @return mixed|null
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCustomerStatus()
    {
        $customer = $this->customerRepository->getById($this->customerSession->getCustomerId());
        /** @var \Magento\Framework\Api\AttributeValue $customerStatus */
        $customerStatus = $customer->getCustomAttribute('customer_status');
        return (isset($customerStatus)) ? $customerStatus->getValue(): null;
    }
}
